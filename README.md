# ViennaRSS

[ViennaRSS](https://www.vienna-rss.com/)의 Style (Theme) 파일 등등

## MyPapes.viennastyle

`/Applications/Vienna.app/Contents/SharedSupport/Styles/Papes.viennastyle` 를 기본으로 수정해서 만들었습니다.

![My Papes](images/viennarss001.png)

### 사용 폰트

  - D2Coding : 메뉴등
  - IropkeBatangOTFM : 본문
  - NanumBarunGothicOTFBold : 제목

### 설치

  - 소스 받기
  ```shell
  % git clone git@gitlab.com:dh.jang/viennarss.git
  % cd viennarss
  ```
  
  - My Pages 설치
  ```shell
  viennarss % open MyPapes.viennastyle
  ```
  
### Licensing

  [Apache License, Version 2.0.](LICENCE.md)
